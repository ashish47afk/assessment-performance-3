import * as React from "react";
import { Chart } from "react-google-charts";
 
const ExampleChart = () => {
  return (
    <Chart
  width={'1400px'}
  height={'400px'}
  chartType="LineChart"
  loader={<div>Loading Chart</div>}
  data={[
    ['x', 'Low' , 'Medium', 'Distinction'],
    ["Americas BFS Major Accounts", 79.25, 17.48, 3.28],
    ["Americas BFS NA", 53.21, 39.45, 7.34],
    ["Americas EIB", 61.24, 31.78, 6.98],
    ["Americas HLS", 62.72, 31.56, 5.72],
    ["Americas TMT East", 53.41, 40.65, 5.93],
    ["Americas TMT West", 53.75, 41.25, 5],
    ["GCTS", 32.43, 40.83, 24.79],
    ["EME-A APAC", 73.20, 22.68, 4.12],
    ["EME-A EME", 67.93, 27.79, 4.28],
    ["Available Pool", 58.88, 34.43, 6.67],
  ]}
  options={{
    hAxis: {
      title: '',
    },
    vAxis: {
      title: '',
      show: 'false'
    },
    series: {
      1: { curveType: 'function' },
    },
    colors: ['#FC2323', '#FFF11B', '#2BE52B'],
          pointSize: 25,
          pointShape: 'square'
  }}
  rootProps={{ 'data-testid': '2' }}
/>  
  );
};
export default ExampleChart;